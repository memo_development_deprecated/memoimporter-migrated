# Memo Importer

### Installation

To install the plugin you can follow the steps described below.

1. Add access key to bitbucket repository.

This can be done in the repository by going to "Repository Settings" and the choose the menu option "Access keys".

2. Add the repository to `composer.json`

If the key `repositories` does not exists yet you will need to create it.
Take a look at the partial `composer.json` and add the missing parts.
```json
{
    "repositories": [
        {
            "type": "path",
            "url": "custom/plugins/*/packages/*",
            "options": {
                "symlink": true
            }
        },
        {
            "type": "path",
            "url": "custom/static-plugins/*",
            "options": {
                "symlink": true
            }
        },
        {
            "type": "vcs",
            "url": "git@bitbucket.org:memo_development/memoimporter.git"
        }
    ]
}
```

After you adapted the `composer.json` file you can require the plugin using composer with the following command.

```bash
composer require memo/importer
```

Now the Plugin should be available in the Shopware Backend.

### Configuration

1. Converter configuration

In this card you can add the Sales Channels which should be added to the Import File.

2. Importer configuration

Here you can set the import profiles for using the direct import option of the converter.
If you want to use a custom profile you will need to create the new custom profile first and then run the following SQL Command directly in the DB.
Otherwise the Profiles will show up as a blank line.
```sql
UPDATE import_export_profile AS p SET p.name = (SELECT pt.label FROM import_export_profile_translation AS pt WHERE pt.`import_export_profile_id` = p.id limit 1) WHERE p.name IS NULL;
```

### Usage

See the [Importer example file](/documentation/import_example.csv) for the required CSV structure.

To run the converter you can use the following console command:
```bash
bin/console memo:import:converter /path/to/import-csv/example.csv /path/to/result/folder/
```

To directly import the files using the converter you can add the parameter `-i`.
When adding this parameter the converter will directly import the files with the import profiles set in the plugin configuration.
This Feature is currently __not implemented__ and a to do.
