# Changes

##### Unreleased

##### 1.1.1

- FEATURE: Added fix for when csv files get too large shopware will not be able to import them.
- FEATURE: Added possibility to remove fields from finished CSV to save on RAM and diskspace.

##### 1.1.0

- FEATURE: Added custom code in backend functionality.

##### 1.0.2

- BUGFIX: Fixed Composer versioning.

##### 1.0.1

- ENHANCEMENT: Added plugin documentation.

##### 1.0.0

- FEATURE: Add plugin configuration and functionality.

##### 0.0.1

- FEATURE: Basic Plugin Structure.
