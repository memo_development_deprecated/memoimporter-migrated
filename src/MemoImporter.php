<?php declare(strict_types=1);

namespace Memo\Importer;

use Shopware\Core\Framework\Plugin;
use Shopware\Core\Framework\Plugin\Context\UninstallContext;
use Shopware\Core\Framework\Plugin\Context\InstallContext;

class MemoImporter extends Plugin
{
    /**
     * @param InstallContext $context
     */
    public function install(Plugin\Context\InstallContext $context): void
    {

    }

    /**
     * @param UninstallContext $context
     */
    public function uninstall(UninstallContext $context): void
    {
        if ($context->keepUserData()) {
            return;
        }
    }
}
