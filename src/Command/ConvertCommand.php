<?php declare(strict_types=1);

namespace Memo\Importer\Command;

use \DateTime;
use \DateInterval;
use Memo\Importer\Services\ImporterService;
use Shopware\Core\Content\ImportExport\ImportExportFactory;
use Shopware\Core\Content\ImportExport\Service\ImportExportService;
use Shopware\Core\Content\ImportExport\Struct\Progress;
use Shopware\Core\Framework\Context;
use Shopware\Core\System\SystemConfig\SystemConfigService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class ConvertCommand extends Command
{
    protected static $defaultName = 'memo:import:converter';

    private ImporterService $importerService;

    private ImportExportFactory $importExportFactory;

    private ImportExportService $initiationService;

    /**
     * @var string|null
     */
    private $productProfileId;

    /**
     * @var string|null
     */
    private $propertiesProfileId;

    /**
     * @var string|null
     */
    private $categoriesProfileId;

    /**
     * @var string|null
     */
    private $variantConfigProfileId;

    /**
     * CalcTurnoverCommand constructor.
     *
     * @param ImporterService $importerService
     */
    public function __construct(
        SystemConfigService $systemConfigService,
        ImporterService $importerService,
        ImportExportService $initiationService,
        ImportExportFactory $importExportFactory
    ) {
        $this->importerService = $importerService;
        $this->initiationService = $initiationService;
        $this->importExportFactory = $importExportFactory;
        $this->productProfileId = $systemConfigService->get('MemoImporter.config.importProfileProducts');
        $this->propertiesProfileId = $systemConfigService->get('MemoImporter.config.importProfileProperties');
        $this->categoriesProfileId = $systemConfigService->get('MemoImporter.config.importProfileCategories');
        $this->variantConfigProfileId = $systemConfigService->get('MemoImporter.config.importProfileVariantConfig');

        parent::__construct();
    }

    /**
     * Configure Command.
     */
    protected function configure()
    {
        $this
            ->setDescription('Converts Import File to a Shopware Import File')
            ->addArgument('input', InputArgument::REQUIRED, 'Input file to convert')
            ->addArgument('output', InputArgument::REQUIRED, 'Output Folder to save the converted files to.')
            ->addOption('import', 'i', InputOption::VALUE_NONE, 'Directly import all files with correct profile. The correct profiles must be set in plugin configuration!')
            ->setHelp('This Importer converts the given CSV File into a Shopware readable import file.');
    }

    /**
     * Execute Command.
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $directlyImport = $input->getOption('import');
        $this->io = new SymfonyStyle($input, $output);
        $inputFilePath = $input->getArgument('input');
        $this->io->title('Import Conversion powered by Media Motion AG');
        $this->io->writeln('Starting conversion for given file: ' . $inputFilePath);
        $error = false;

        $importResult = $this->importerService->execute($inputFilePath, $input->getArgument('output'));

        if (count($importResult['errors'])) {
            $this->io->error('There have been errors during conversion!');

            foreach ($importResult['errors'] as $error) {
                $this->io->error($error);
            }

            $error = true;
        } else {
            $this->io->success('Conversion completed successfully');
        }

        if (count($importResult['files']) > 0) {
            $this->io->writeln('The following files have been written:');

            foreach ($importResult['files'] as $file) {
                $this->io->writeln($file['path']);
            }

            $this->io->writeln('These should be imported in the given order!');
        }

        if ($directlyImport && !$error) {
            $this->io->writeln('Starting direct import of the converted Files.');
            $directImportError = false;

            foreach ($importResult['files'] as $file) {
                $this->io->writeln('Importing file: ' . $file['path'] . ' with type: ' . $file['type']);

                $profile = false;

                switch ($file['type']) {
                    case ImporterService::IMPORT_TYPE_PRODUCTS:
                        $profile = $this->productProfileId;
                        break;
                    case ImporterService::IMPORT_TYPE_CATEGORY:
                        $profile = $this->categoriesProfileId;
                        break;
                    case ImporterService::IMPORT_TYPE_VARIANT_CONFIG:
                        $profile = $this->variantConfigProfileId;
                        break;
                    case ImporterService::IMPORT_TYPE_PROPERTIES:
                        $profile = $this->propertiesProfileId;
                        break;
                }

                if (!$profile) {
                    $this->io->error('No import profile configured for type: ' . $file['type']);
                    $this->io->writeln('Stopping import due to error.');
                    $directImportError = true;
                    $error = true;

                    break;
                }

                $tmpError = $this->importFile($file['path'], $profile);

                if (!$tmpError) {
                    $error = true;
                    $directImportError = true;
                    $this->io->writeln('Stopping import due to error.');

                    break;
                }
            }

            if (!$directImportError) {
                $this->io->success('Imported all files successfully!');
            }
        }

        if ($error) {
            return 1;
        } else {
            return 0;
        }
    }

    private function importFile($filePath, $profileId) {
        $file = new UploadedFile($filePath, basename($filePath), 'text/csv');
        $expireDate = new DateTime();
        $expireDate->add(new DateInterval('P5D'));

        $log = $this->initiationService->prepareImport(
            Context::createDefaultContext(),
            $profileId,
            $expireDate,
            $file
        );

        $startTime = time();

        $importExport = $this->importExportFactory->create($log->getId());

        $total = filesize($filePath);
        if ($total === false) {
            $total = 0;
        }
        $progressBar = $this->io->createProgressBar($total);

        $this->io->title(sprintf('Starting import of size %d ', $total));

        $records = 0;

        $progress = new Progress($log->getId(), Progress::STATE_PROGRESS, 0);
        do {
            $progress = $importExport->import(Context::createDefaultContext(), $progress->getOffset());
            $progressBar->setProgress($progress->getOffset());
            $records += $progress->getProcessedRecords();
        } while (!$progress->isFinished());

        $elapsed = time() - $startTime;
        $this->io->newLine(2);

//        if ($printErrors) {
//            $this->printErrors($importExport, $log, $this->io, $doRollback && $progress->getState() === Progress::STATE_FAILED);
//        }

//        $this->printResults($log, $this->io);

        if ($progress->getState() === Progress::STATE_SUCCEEDED) {
            $this->io->success(sprintf('Successfully imported %d records in %d seconds', $records, $elapsed));

            return true;
        }

        $this->io->warning('Not all records could be imported due to errors');

        return false;
    }
}
