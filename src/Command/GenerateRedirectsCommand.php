<?php declare(strict_types=1);

namespace Memo\Importer\Command;

use Memo\Importer\Services\RedirectService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class GenerateRedirectsCommand extends Command
{
    protected static $defaultName = 'memo:import:redirects';

    /**
     * @var RedirectService
     */
    protected $redirectService;

    /**
     * CalcTurnoverCommand constructor.
     *
     * @param RedirectService $redirectService
     */
    public function __construct(RedirectService $redirectService)
    {
        $this->redirectService = $redirectService;

        parent::__construct();
    }

    /**
     * Configure Command.
     */
    protected function configure()
    {
        $this
            ->setDescription('Converts redirect file from old website/shop to a .htaccess file')
            ->addArgument('input', InputArgument::REQUIRED, 'Input file to convert')
            ->addArgument('output', InputArgument::REQUIRED, 'Output file to save the converted .htaccess to.')
            ->addArgument('salesChannelName', InputArgument::REQUIRED, 'Name of the sales channel.')
            ->addOption('permanentRedirects', 'p', InputOption::VALUE_NONE, 'Redirect permanently.')
            ->addArgument('languageName', InputArgument::REQUIRED, 'Name of the language.');
    }

    /**
     * Execute Command.
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->io = new SymfonyStyle($input, $output);
        $inputFilePath = $input->getArgument('input');
        $salesChannelName = $input->getArgument('salesChannelName');
        $languageName = $input->getArgument('languageName');
        $permanentRedirects = $input->getOption('permanentRedirects');
        $this->io->title('URL redirect generator powered by Media Motion AG');
        $this->io->writeln('Starting conversion for given file: ' . $inputFilePath);
        $error = false;

        $importResult = $this->redirectService->execute($inputFilePath, $input->getArgument('output'), $salesChannelName, $languageName, $permanentRedirects);

        if (count($importResult['errors'])) {
            $this->io->error('There have been errors during conversion!');

            foreach ($importResult['errors'] as $error) {
                $this->io->error($error);
            }

            $error = true;
        } else {
            $this->io->success('Conversion completed successfully');
        }

        if ($importResult['file'] !== false) {
            $this->io->writeln('The following file has been written: ' . $importResult['file']);
        }

        if ($error) {
            return 1;
        } else {
            return 0;
        }
    }
}
