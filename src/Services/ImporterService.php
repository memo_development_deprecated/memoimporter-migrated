<?php
/**
 * Implemented by Media Motion AG team https://www.mediamotion.ch
 *
 * @copyright Media Motion AG https://www.mediamotion.ch
 * @license LGPL-3.0+
 * @link https://www.mediamotion.ch
 */

namespace Memo\Importer\Services;

use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepository;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsFilter;
use Shopware\Core\Framework\Uuid\Uuid;
use Shopware\Core\System\SystemConfig\SystemConfigService;

/**
 * Class ImporterService
 *
 * @package MemoImporter\Services
 */
class ImporterService
{
    const CSV_WRITE_LIMIT = 500;

    const IMPORT_TYPE_CATEGORY = 'category';

    const IMPORT_TYPE_PROPERTIES = 'properties';

    const IMPORT_TYPE_PRODUCTS = 'products';

    const IMPORT_TYPE_VARIANT_CONFIG = 'variants_config';

    /**
     * @var EntityRepository
     */
    private $productRepository;

    /**
     * @var EntityRepository
     */
    private $taxRepository;

    /**
     * @var EntityRepository
     */
    private $manufacturerRepository;

    /**
     * @var EntityRepository
     */
    private $propertyGroupOptionRepository;

    /**
     * @var EntityRepository
     */
    private $propertyGroupRepository;

    /**
     * @var EntityRepository
     */
    private $categoryRepository;

    /**
     * @var EntityRepository
     */
    private $variantConfigRepository;

    /**
     * @var array
     */
    private $articleIds = [];

    /**
     * @var array
     */
    private $taxRateIds = [];

    /**
     * @var array
     */
    private $manufacturerIds = [];

    /**
     * @var array
     */
    private $propertyIds = [];

    /**
     * @var array
     */
    private $propertyGroupIds = [];

    /**
     * @var array
     */
    private $newProperties = [];

    /**
     * @var array
     */
    private $newCategoriesTree = [];

    /**
     * @var array
     */
    private $salesChannelIds;

    /**
     * @var Context
     */
    private $context;

    /**
     * @var array
     */
    private $variantConfig = [];

    /**
     * @var array
     */
    private $fieldsRemoveInlineStyles;

    /**
     * @var array
     */
    private $fieldsConvertHTMLEntities;

    /**
     * @var array
     */
    private $customFieldManipulations = [];

    /**
     * @var string
     */
    private $customDataManipulation = '';

    /**
     * @var string
     */
    private $newCategoryLayoutId = '';

    /**
     * @var array
     */
    private $customFieldsSelection = [];

    /**
     * @var array
     */
    private $fieldsRemoveFromCSV = [];

    /**
     * ImporterService constructor.
     *
     * @param SystemConfigService $systemConfigService
     * @param EntityRepository $productRepository
     * @param EntityRepository $taxRepository
     * @param EntityRepository $manufacturerRepository
     * @param EntityRepository $propertyGroupOptionRepository
     * @param EntityRepository $propertyGroupRepository
     * @param EntityRepository $categoryRepository
     * @param EntityRepository $variantConfigRepository
     */
    public function __construct(
        SystemConfigService       $systemConfigService,
        EntityRepository $productRepository,
        EntityRepository $taxRepository,
        EntityRepository $manufacturerRepository,
        EntityRepository $propertyGroupOptionRepository,
        EntityRepository $propertyGroupRepository,
        EntityRepository $categoryRepository,
        EntityRepository $variantConfigRepository
    )
    {
        $this->productRepository = $productRepository;
        $this->taxRepository = $taxRepository;
        $this->manufacturerRepository = $manufacturerRepository;
        $this->salesChannelIds = $systemConfigService->get('MemoImporter.config.importSalesChannelId');
        $fieldsRemoveInlineStyles = $systemConfigService->get('MemoImporter.config.importRemoveInlineStylesFields');
        $fieldsRemoveFromCSV = $systemConfigService->get('MemoImporter.config.removeCSVFieldsAfterDataManipulation');
        $fieldsConvertHTMLEntities = $systemConfigService->get('MemoImporter.config.importReplaceHTMLEntitiesFields');
        $this->newCategoryLayoutId = is_null($systemConfigService->get('MemoImporter.config.categoryLayoutId')) ? '' : $systemConfigService->get('MemoImporter.config.categoryLayoutId');
        $this->customFieldsSelection = is_null($systemConfigService->get('MemoImporter.config.customFieldsData')) ? [] : json_decode($this->removeSpecialChars($systemConfigService->get('MemoImporter.config.customFieldsData')), true);
        $this->customDataManipulation = is_null($systemConfigService->get('MemoImporter.config.customDataManipulation')) ? '' : $systemConfigService->get('MemoImporter.config.customDataManipulation');
        $this->fieldsRemoveInlineStyles = is_null($fieldsRemoveInlineStyles) || trim($fieldsRemoveInlineStyles) === '' ? [] : explode(',', $fieldsRemoveInlineStyles);
        $this->fieldsRemoveFromCSV = is_null($fieldsRemoveFromCSV) || trim($fieldsRemoveFromCSV) === '' ? [] : explode(',', $fieldsRemoveFromCSV);
        $this->fieldsConvertHTMLEntities = is_null($fieldsConvertHTMLEntities) || trim($fieldsConvertHTMLEntities) === '' ? [] : explode(',', $fieldsConvertHTMLEntities);
        $this->onlyImportNewArticles = is_null($systemConfigService->get('MemoImporter.config.onlyImportNewArticles')) ? false : $systemConfigService->get('MemoImporter.config.onlyImportNewArticles');
        $this->propertyGroupOptionRepository = $propertyGroupOptionRepository;
        $this->propertyGroupRepository = $propertyGroupRepository;
        $this->categoryRepository = $categoryRepository;
        $this->variantConfigRepository = $variantConfigRepository;

        for ($i = 1; $i <= 10; $i++) {
            $settingField = $systemConfigService->get('MemoImporter.config.codeField' . $i);
            $settingCode = $systemConfigService->get('MemoImporter.config.codeExecutable' . $i);

            if (is_null($settingCode) || is_null($settingField)) {
                continue;
            }

            $settingField = trim($settingField);
            $settingCode = trim($settingCode);

            if ($settingField === '' || $settingCode === '') {
                continue;
            }

            $this->customFieldManipulations[] = [
                'fields' => explode(',', $settingField),
                'code' => $settingCode
            ];
        }

        $this->context = Context::createDefaultContext();
    }

    /**
     * Run the conversion of the Import.
     *
     * @param string $inputPath
     * @param string $outputPath
     *
     * @return array{'failed' => boolean, 'errors' => []}
     */
    public function execute(string $inputPath, string $outputPath)
    {
        $result = [
            'failed' => false,
            'errors' => [],
            'files' => []
        ];
        $csvResult = [];
        $csvResultVariants = [];
        $csvData = $this->getCSVArray($inputPath);
        $csvData = $this->executeCustomManipulationsOnData($csvData);
        $outputPath = $this->addTrailingSlash($outputPath);
        $timestamp = date("Ymdhis");

        if (count($csvData) === 0) {
            $result['failed'] = true;
            $result['errors'][] = 'CSV could not be read at Path "' . $inputPath . '"';

            return $result;
        }

        $csvData = $this->groupByParentArticleNumber($csvData);
        $i = 1;

        foreach ($csvData as $mainArticleNumber => $collection) {
            foreach ($collection['main'] as $key => $article) {
                $extendedArticle = $this->insertInfoIntoArticle($article);

                if (count($extendedArticle) === 0) {
                    unset($collection['main'][$key]);
                    continue;
                }

                $collection['main'][$key] = $extendedArticle;
            }

            foreach ($collection['variants'] as $key => $article) {
                $extendedArticle = $this->insertInfoIntoArticle($article);

                if (count($extendedArticle) === 0) {
                    unset($collection['variants'][$key]);
                    continue;
                }

                $collection['variants'][$key] = $extendedArticle;
            }

            $csvData[$mainArticleNumber] = $collection;

            $i++;
        }

        $this->createVariantConfiguration($csvData);

        foreach ($csvData as $data) {
            foreach ($data['main'] as $mainArticle) {
                $csvResult[] = $mainArticle;
            }

            foreach ($data['variants'] as $variant) {
                $csvResultVariants[] = $variant;
            }
        }

        $newCategories = $this->recursivelyCreateCategories();
        $countDocuments = 1;

        foreach ($csvResult as $key => $item) {
            $csvResult[$key] = $this->removeUnusedFields($item);
        }

        if (count($newCategories) > 0) {
            $outputPathCategories = $outputPath . $countDocuments . '_categories_' . $timestamp;

            $filesList = $this->writeImportFilePro($newCategories, $outputPathCategories, self::IMPORT_TYPE_CATEGORY);
            $result['files'] = array_merge($result['files'], $filesList);
            $countDocuments = $countDocuments + count($filesList);
        }

        if (count($csvResult) > 0) {
            $outputPathArticles = $outputPath . $countDocuments . '_articles_' . $timestamp;
            $filesList = $this->writeImportFilePro($csvResult, $outputPathArticles, self::IMPORT_TYPE_PRODUCTS);
            $result['files'] = array_merge($result['files'], $filesList);
            $countDocuments = $countDocuments + count($filesList);
        }

        if (count($this->newProperties) > 0) {
            $outputPathProperties = $outputPath . $countDocuments . '_properties_' . $timestamp;

            $filesList = $this->writeImportFilePro($this->newProperties, $outputPathProperties, self::IMPORT_TYPE_PROPERTIES);
            $result['files'] = array_merge($result['files'], $filesList);
            $countDocuments = $countDocuments + count($filesList);
        }

        if (count($this->variantConfig) > 0) {
            $outputPathArticles = $outputPath . $countDocuments . '_variantConfig_' . $timestamp;
            $filesList = $this->writeImportFilePro($this->variantConfig, $outputPathArticles, self::IMPORT_TYPE_VARIANT_CONFIG);
            $result['files'] = array_merge($result['files'], $filesList);
            $countDocuments = $countDocuments + count($filesList);
        }

        if (count($csvResultVariants) > 0) {
            $outputPathArticles = $outputPath . $countDocuments . '_variants_' . $timestamp;
            $filesList = $this->writeImportFilePro($csvResultVariants, $outputPathArticles, self::IMPORT_TYPE_PRODUCTS);
            $result['files'] = array_merge($result['files'], $filesList);
            $countDocuments = $countDocuments + count($filesList);
        }

        return $result;
    }

    /**
     * Inserts necessary information into article.
     *
     * @param array $article
     *
     * @return array
     */
    private function insertInfoIntoArticle(array $article)
    {
        $idSearchResult = $this->getIdByArticleNumber($article['product_number']);

        if ($idSearchResult['alreadyExists'] && $this->onlyImportNewArticles) {
            return [];
        }

        $article['id'] = $idSearchResult['id'];
        $article['parent_id'] = $this->getIdByArticleNumber($article['main_product_number'])['id'];
        $article['tax_id'] = $this->getTaxRateId($article['tax_rate'], $article['tax_name']);
        $article['manufacturer_id'] = $this->getManufacturerId($article['manufacturer_name']);
        $article['propertyIds'] = $this->getPropertyIds($article['properties']);
        $article['optionIds'] = $this->getPropertyIds($article['options']);
        $article['categories'] = $this->getCategoryIds($article['categories']);
        $article['sales_channel'] = implode('|', $this->salesChannelIds);
        $article['custom_fields'] = $this->createCustomFields($article);

        if ($idSearchResult['alreadyExists']) {
            $article['cover_media_url'] = '';
        }

        return $article;
    }

    /**
     * Returns all entries grouped by parent articlenumber.
     *
     * @param array $data
     * @param string $articleNumberFieldName
     * @param string $parentNumberFieldName
     *
     * @return array
     */
    private function groupByParentArticleNumber(array $data, $articleNumberFieldName = 'product_number', $parentNumberFieldName = 'main_product_number')
    {
        $result = [];

        foreach ($data as $item) {
            $articleNumber = $item[$parentNumberFieldName];
            $isMain = false;

            if ($articleNumber === '' || $articleNumber === false || $articleNumber === $item[$articleNumberFieldName]) {
                $articleNumber = $item[$articleNumberFieldName];
                $isMain = true;
            }

            if (!array_key_exists($articleNumber, $result)) {
                $result[$articleNumber] = [
                    'main' => [],
                    'variants' => []
                ];
            }

            if ($isMain) {
                $result[$articleNumber]['main'][] = $item;
            } else {
                $result[$articleNumber]['variants'][] = $item;
            }
        }

        return $result;
    }

    /**
     * Creates a array from a given CSV with title headers set as keys.
     *
     * @param string $csvPath
     *
     * @return array
     */
    private function getCSVArray(string $csvPath)
    {
        $csvFile = fopen($csvPath, 'r');
        $csvData = [];
        $index = 0;
        $indexes = [];

        if (!$csvFile) {
            return [];
        }

        while (false !== ($row = fgetcsv($csvFile, 0, ';', '"', "\n"))) {
            if ($index === 0) {
                $indexes = $row;
            } else {
                $data = [];

                foreach ($indexes as $key => $indexed) {
                    // Remove route params.
                    $item = strtok($row[$key], '?');
                    $item = in_array($indexed, $this->fieldsRemoveInlineStyles) ? $this->removeInlineStyles($item) : $item;
                    $item = in_array($indexed, $this->fieldsConvertHTMLEntities) ? $this->decodeHtmlEntities($item) : $item;

                    if ($indexed === 'product_number' || $indexed === 'main_product_number') {
                        $item = $this->fixArticleNumber($item);
                    }

                    $item = $this->executeCustomManipulationsOnFields($item, $indexed);

                    $data[$indexed] = $item;
                }

                if (count($data) !== 0) {
                    $csvData[] = $data;
                }
            }

            $index++;
        }

        return $csvData;
    }

    /**
     * Fixes Article Numbers for Shopware Import.
     *
     * @param string $articleNumber
     *
     * @return string
     */
    private function fixArticleNumber(string $articleNumber)
    {
        return preg_replace('/[^a-zA-Z0-9-_.]/', '', $articleNumber);
    }

    /**
     * Writes Import File the Pro way.
     *
     * @param array $data
     * @param string $filePath
     * @param string $importType
     *
     * @return array
     */
    private function writeImportFilePro(array $data, string $filePath, string $importType)
    {
        $firstRow = $this->getFirstValueInArray($data);

        return $this->recursivelyWriteImportFile($firstRow, $data, $filePath, $importType);
    }

    /**
     * This is used to split files into multiple, when too much data is written.
     *
     * @param array $header
     * @param array $data
     * @param string $filePath
     * @param string $importType
     * @param int $index
     *
     * @return array
     */
    private function recursivelyWriteImportFile(array $header, array $data, string $filePath, string $importType, int $index = 0)
    {
        $fileList = [];
        $tmpFilePath = $filePath . '_' . $index . '.csv';
        $outputFile = fopen($tmpFilePath, 'w');
        $lineCount = 0;
        $index++;
        $fileList[] = [
            'path' => $tmpFilePath,
            'type' => $importType
        ];

        if (is_array($header)) {
            fputcsv($outputFile, array_keys($header), ';');
        }

        foreach ($data as $key => $row) {
            $lineCount++;
            fputcsv($outputFile, $row, ';');
            unset($data[$key]);

            if ($lineCount >= self::CSV_WRITE_LIMIT) {
                break;
            }
        }

        fclose($outputFile);

        if (count($data) > 0) {
            $fileList = array_merge($fileList, $this->recursivelyWriteImportFile($header, $data, $filePath, $importType, $index));
        }

        return $fileList;
    }

    /**
     * Find ID for article if already exists, else we create a new ID.
     *
     * @param $articleNumber
     *
     * @return array
     */
    private function getIdByArticleNumber($articleNumber)
    {
        if ($articleNumber === false || $articleNumber === '') {
            return ['id' => '', 'alreadyExists' => false];
        }

        if (array_key_exists($articleNumber, $this->articleIds)) {
            return ['id' => $this->articleIds[$articleNumber], 'alreadyExists' => true];
        }

        $criteria = (new Criteria())->addFilter(new EqualsFilter('productNumber', $articleNumber));

        $entities = $this->productRepository->search($criteria, $this->context)->getEntities();

        if ($entities->count() === 0) {
            $newId = Uuid::randomHex();
            $this->articleIds[$articleNumber] = $newId;

            return ['id' => $this->articleIds[$articleNumber], 'alreadyExists' => false];
        }

        $alreadyExistingId = $entities->first()->getId();
        $this->articleIds[$articleNumber] = $alreadyExistingId;

        return ['id' => $alreadyExistingId, 'alreadyExists' => true];
    }

    /**
     * Find tax ID by a given rate and a given name.
     *
     * @param $taxRate
     * @param $taxName
     *
     * @return string
     */
    private function getTaxRateId($taxRate, $taxName)
    {
        if ($taxRate === false || $taxRate === '') {
            return '';
        }

        if (array_key_exists($taxRate, $this->taxRateIds)) {
            foreach ($this->taxRateIds[$taxRate] as $taxRate) {
                if ($taxRate['name'] === $taxName) {
                    return $taxRate['id'];
                }
            }
        }

        $criteria = (new Criteria())->addFilter(new EqualsFilter('taxRate', floatval($taxRate)));
        $criteria->addFilter(new EqualsFilter('name', $taxName));
        $entities = $this->taxRepository->search($criteria, $this->context)->getEntities();

        if ($entities->count() === 0) {
            $taxRateId = Uuid::randomHex();

            $this->taxRateIds[$taxRate] = [
                'name' => $taxName,
                'id' => $taxRateId
            ];

            return $taxRateId;
        }

        $id = $entities->first()->getId();
        $this->taxRateIds[$taxRate][] = [
            'id' => $id,
            'name' => $taxName
        ];

        return $id;
    }

    /**
     * Find the correct property IDs.
     *
     * @param $strProperties
     *
     * @return string
     */
    private function getPropertyIds($strProperties)
    {
        if ($strProperties === false || $strProperties === '') {
            return '';
        }

        $propertiesTmp = explode('|', $strProperties);
        $properties = [];

        // Split properties into array.
        foreach ($propertiesTmp as $tmp) {
            $tmpArray = explode(':', $tmp);

            $properties[] = [
                'group' => $tmpArray[0],
                'property' => $tmpArray[1]
            ];
        }

        $result = [];

        // Check if properties have already been created.
        foreach ($properties as $key => $property) {
            if (
                array_key_exists($property['group'], $this->propertyIds)
                && array_key_exists($property['property'], $this->propertyIds[$property['group']])
            ) {
                $result[] = $this->propertyIds[$property['group']][$property['property']];

                unset($properties[$key]);
            }
        }

        // If all properties have been found -> return result.
        if (count($properties) === 0) {
            return implode('|', $result);
        }

        // Find already existing properties in DB.
        foreach ($properties as $key => $property) {
            $criteria = (new Criteria())->addAssociations(['translations', 'group', 'group.translations']);
            $criteria->addFilter(new EqualsFilter('translations.name', $property['property']));
            $criteria->addFilter(new EqualsFilter('group.translations.name', $property['group']));
            $entities = $this->propertyGroupOptionRepository->search($criteria, $this->context)->getEntities();

            if ($entities->count() === 0) {
                continue;
            }

            $result[] = $entities->first()->getId();
            unset($properties[$key]);
        }

        // If all properties have been found -> return result.
        if (count($properties) === 0) {
            return implode('|', $result);
        }

        // Create new properties for the missing properties using the import file.
        foreach ($properties as $key => $property) {
            $groupId = Uuid::randomHex();

            // Check if the ID of this group has already been found in DB.
            if (array_key_exists($property['group'], $this->propertyGroupIds)) {
                $groupId = $this->propertyGroupIds[$property['group']];
            } else {
                // If not already found check DB.
                $criteria = (new Criteria())->addAssociations(['translations']);
                $criteria->addFilter(new EqualsFilter('translations.name', $property['group']));
                $entities = $this->propertyGroupRepository->search($criteria, $this->context)->getEntities();

                if ($entities->count() !== 0) {
                    $groupId = $entities->first()->getId();
                }
            }

            $propertyId = Uuid::randomHex();

            $this->newProperties[] = [
                'id' => $propertyId,
                'color_hex_code' => '',
                'name' => $property['property'],
                'position' => 1,
                'group_id' => $groupId,
                'group_display_type' => 'text',
                'group_sorting_type' => 'position',
                'group_name' => $property['group'],
                'group_description' => '',
                'group_position' => 1,
                'media_id' => '',
                'media_url' => '',
                'media_folder_id' => '',
                'media_type' => '',
                'media_title' => '',
                'media_alt' => '',
            ];

            $this->propertyIds[$property['group']][$property['property']] = $propertyId;
            $this->propertyGroupIds[$property['group']] = $groupId;
            $result[] = $propertyId;
        }

        return implode('|', $result);
    }

    /**
     * Find the correct manufacturer IDs.
     *
     * @param $manufacturerName
     *
     * @return string
     */
    private function getManufacturerId($manufacturerName)
    {
        // Check if field is set.
        if ($manufacturerName === false || $manufacturerName === '') {
            return '';
        }

        // If manufacturer has already been found in DB or freshly created use the given ID.
        if (array_key_exists($manufacturerName, $this->manufacturerIds)) {
            return $this->manufacturerIds[$manufacturerName];
        }

        // Search DB for manufacturer.
        $criteria = (new Criteria())->addAssociations(['translations'])->addFilter(new EqualsFilter('translations.name', $manufacturerName));
        $entities = $this->manufacturerRepository->search($criteria, $this->context)->getEntities();

        if ($entities->count() > 0) {
            $id = $entities->first()->getId();
            $this->manufacturerIds[$manufacturerName] = $id;

            return $id;
        }

        // Manufacturer does not yet exist, therefore we create a new ID.
        $newId = Uuid::randomHex();
        $this->manufacturerIds[$manufacturerName] = $newId;

        return $newId;
    }

    /**
     * Find the correct category IDs.
     *
     * @param $strCategories
     *
     * @return string
     */
    private function getCategoryIds($strCategories)
    {
        if ($strCategories === false || $strCategories === '') {
            return '';
        }

        $categoriesTmp = explode('|', $strCategories);
        $categories = [];

        // Create a category tree by using dark magic.
        foreach ($categoriesTmp as $tmp) {
            // This will return array like ['Sub Category', 'Middle Category', 'Top Level Category']
            $tmpArray = array_reverse(explode('>', $tmp));
            $currentArray = [];

            // Loop through a single category list.
            foreach ($tmpArray as $item) {
                $newArray = [];
                $val = $currentArray;

                // If this is the first time we go through this list we want an empty string.
                if (count($currentArray) === 0) {
                    $val = '';
                }

                // Inserts the value into the temporary array.
                // $newArray will look like this afterwards: ['Middle Category' => ['Sub Category' => '']]
                $newArray[trim($item)] = $val;

                $currentArray = $newArray;
            }

            // Merge the result recursively, so that we get the tree.
            // Result will look like the following:
            // array:1 [
            //     "Home" => array:2 [
            //         "Stoffwelten" => array:1 [
            //             "Stoffe zum Färben" => ""
            //         ]
            //         "Sommerstrick von Stoffonkel" => ""
            //     ]
            // ]
            $categories = array_merge_recursive($categories, $currentArray);
        }

        $result = $this->recursivelyCheckIfCategoryExists($categories);

        $this->newCategoriesTree = $this->recursivelyMergeCategoriesToExisting($result);

        return implode('|', $this->flattenCategoryTree($result));
    }

    /**
     * This function will search recursively for existing categories and add IDs to them.
     * If the ID is '' it means, that this category has not been found in the given parent.
     * If parentId is null this means, that we have a top level category.
     *
     * @param $categoryTree
     * @param string | null $parentId
     *
     * @return array
     */
    private function recursivelyCheckIfCategoryExists($categoryTree, $parentId = null, $alreadyLoadedCategories = null)
    {
        $result = [];

        if (is_null($alreadyLoadedCategories)) {
            $alreadyLoadedCategories = $this->newCategoriesTree;
        }

        foreach ($categoryTree as $category => $categoryItemTree) {
            $exists = true;
            $newAlreadyLoadedCategories = [];

            // Check if category ID has already been loaded or created for this Category.
            if (array_key_exists($category, $alreadyLoadedCategories)) {
                $categoryId = $alreadyLoadedCategories[$category]['id'];
                $exists = $alreadyLoadedCategories[$category]['exists'];
                $newAlreadyLoadedCategories = $alreadyLoadedCategories[$category]['categories'];
            } else {
                $categoryId = $this->getIdForCategoryIfExists($category, $parentId);
            }

            $newCategoryId = $categoryId;

            if ($newCategoryId === '') {
                $newCategoryId = Uuid::randomHex();
                $exists = false;
            }

            $result[$category]['id'] = $newCategoryId;
            $result[$category]['exists'] = $exists;

            if (is_array($categoryItemTree)) {
                $result[$category]['categories'] = $this->recursivelyCheckIfCategoryExists($categoryItemTree, $categoryId, $newAlreadyLoadedCategories);
            } else {
                $result[$category]['categories'] = [];
            }
        }

        return $result;
    }

    /**
     * Get Category ID from DB if the category exists, if the $parentId is not null and not '' it will also check by parent category.
     * If parentId = '' this function will return a empty string without checking the DB, as this means that the parent category has not been found.
     *
     * @param $categoryName
     * @param string | null $parentId
     *
     * @return string
     */
    private function getIdForCategoryIfExists($categoryName, $parentId = null)
    {
        if ($parentId === '') {
            return '';
        }

        // Search DB for manufacturer.
        $criteria = (new Criteria())
            ->addAssociations(['translations', 'parent'])
            ->addFilter(new EqualsFilter('translations.name', $categoryName))
            ->addFilter(new EqualsFilter('parent.id', $parentId));

        $entities = $this->categoryRepository->search($criteria, $this->context)->getEntities();

        if ($entities->count() > 0) {
            return $entities->first()->getId();
        }

        return '';
    }

    /**
     * Flattens the given category tree to single dimension array.
     *
     * @param array $categoryTree
     *
     * @return array
     */
    private function flattenCategoryTree($categoryTree)
    {
        $result = [];

        foreach ($categoryTree as $category => $categoryItemTree) {
            if (count($categoryItemTree['categories']) > 0) {
                $items = $this->flattenCategoryTree($categoryItemTree['categories']);

                foreach ($items as $item) {
                    array_push($result, $item);
                }
            } else {
                $result[] = $categoryItemTree['id'];
            }
        }

        return $result;
    }

    /**
     * Recursively creates a array for the new categories to be imported.
     *
     * @param array | null $currentTree
     * @param string | null $parentId
     *
     * @return array
     */
    private function recursivelyCreateCategories($currentTree = null, $parentId = null)
    {
        if (is_null($currentTree)) {
            $currentTree = $this->newCategoriesTree;
        }

        if (is_null($parentId)) {
            $parentId = '';
        }

        $result = [];

        foreach ($currentTree as $categoryName => $categoryValues) {
            if ($categoryValues['exists'] === false) {
                $result[] = [
                    'id' => $categoryValues['id'],
                    'parent_id' => $parentId,
                    'active' => 1,
                    'type' => 'page',
                    'visible' => 1,
                    'name' => $categoryName,
                    'external_link' => '',
                    'description' => '',
                    'meta_title' => '',
                    'meta_description' => '',
                    'media_id' => '',
                    'media_url' => '',
                    'media_folder_id' => '',
                    'media_type' => '',
                    'media_title' => '',
                    'media_alt' => '',
                    'cms_page_id' => $this->newCategoryLayoutId,
                ];
            }

            $subCategories = $this->recursivelyCreateCategories($categoryValues['categories'], $categoryValues['id']);

            foreach ($subCategories as $subCategory) {
                array_push($result, $subCategory);
            }
        }

        return $result;
    }

    /**
     * @param array $newCategories
     * @param null $alreadyLoadedCategories
     *
     * @return array|mixed|null
     */
    private function recursivelyMergeCategoriesToExisting(array $newCategories, $alreadyLoadedCategories = null)
    {
        if (is_null($alreadyLoadedCategories)) {
            $alreadyLoadedCategories = $this->newCategoriesTree;
        }

        foreach ($newCategories as $categoryName => $newCategory) {
            $alreadyLoadedCategoriesNew = [];

            if (array_key_exists($categoryName, $alreadyLoadedCategories)) {
                $alreadyLoadedCategoriesNew = $alreadyLoadedCategories[$categoryName]['categories'];

                // Category already exists and ist empty.
                if (count($newCategory['categories']) === 0) {
                    continue;
                }
                // If Category is not yet loaded, and there are no remaining children to be rendered we continue to next item.
            } elseif (count($newCategory['categories']) === 0) {
                $alreadyLoadedCategories[$categoryName] = $newCategory;
                continue;
            }

            $alreadyLoadedCategories[$categoryName] = [
                'id' => $newCategory['id'],
                'exists' => $newCategory['exists'],
                'categories' => $this->recursivelyMergeCategoriesToExisting($newCategory['categories'], $alreadyLoadedCategoriesNew)
            ];
        }

        return $alreadyLoadedCategories;
    }

    /**
     * @param array $articleContainers
     */
    private function createVariantConfiguration(array $articleContainers)
    {
        $config = [];

        foreach ($articleContainers as $articleContainer) {
            foreach ($articleContainer['variants'] as $variant) {
                $options = explode('|', $variant['optionIds']);

                foreach ($options as $option) {
                    // Check if this option - product combination has already been added to config.
                    if (array_key_exists($variant['parent_id'], $config) && array_key_exists($option, $config[$variant['parent_id']])) {
                        continue;
                    }

                    $configId = Uuid::randomHex();

                    // Search DB for variant configuration.
                    $criteria = (new Criteria())
                        ->addFilter(new EqualsFilter('productId', $variant['parent_id']))
                        ->addFilter(new EqualsFilter('optionId', $option));

                    $entities = $this->variantConfigRepository->search($criteria, $this->context)->getEntities();

                    if ($entities->count() > 0) {
                        $configId = $entities->first()->getId();
                    }

                    $config[$variant['parent_id']][$option] = [
                        'id' => $configId,
                        'product_id' => $variant['parent_id'],
                        'option_id' => $option,
                        'position' => 1,
                        'media_id' => '',
                        'media_url' => '',
                        'media_folder_id' => '',
                        'media_type' => '',
                        'media_title' => '',
                        'media_alt' => '',
                        'price_net' => '',
                        'price_gross' => '',
                    ];
                }
            }
        }

        foreach ($config as $options) {
            foreach ($options as $option) {
                $this->variantConfig[] = $option;
            }
        }
    }

    /**
     * Adds a trailing slash to a given string if there is no trailing slash already.
     *
     * @param string $path
     *
     * @return string
     */
    private function addTrailingSlash(string $path)
    {
        $lastchar = $path[-1];

        if (strcmp($lastchar, "/") === 0) {
            return $path;
        }

        return $path . '/';
    }

    /**
     * Removes inline styles from a string of html.
     *
     * @param string $html
     *
     * @return string
     */
    private function removeInlineStyles(string $html): string
    {
        return preg_replace('#(<[a-z ]*)(style=("|\')(.*?)("|\'))([a-z ]*>)#', '\\1\\6', $html);
    }

    /**
     * Finds the first value inside an Array.
     *
     * @param array $array
     *
     * @return mixed|null
     */
    private function getFirstValueInArray(array $array)
    {
        $tmp = array_values($array);

        if (!array_key_exists(0, $tmp)) {
            return null;
        }

        return $tmp[0];
    }

    /**
     * Executes the custom manipulations for the given field.
     *
     * @param string $fieldValue
     * @param string $fieldKey
     *
     * @return string
     */
    private function executeCustomManipulationsOnFields(string $fieldValue, string $fieldKey): string
    {
        foreach ($this->customFieldManipulations as $customManipulation) {
            if (!in_array($fieldKey, $customManipulation['fields'])) {
                continue;
            }

            eval($customManipulation['code']);
        }

        return $fieldValue;
    }

    /**
     * Executes the custom manipulations for the given field.
     *
     * @param array $csvData
     *
     * @return array
     */
    private function executeCustomManipulationsOnData(array $csvData): array
    {
        if ($this->customDataManipulation === '') {
            return $csvData;
        }

        eval($this->customDataManipulation);

        return $csvData;
    }

    /**
     * Converts HTML Entities (e.g. &auml;) to regular a regular text.
     *
     * @param string $string
     *
     * @return string
     */
    private function decodeHtmlEntities(string $string)
    {
        return html_entity_decode($string, ENT_COMPAT, 'UTF-8');
    }

    /**
     * @param array $item
     *
     * @return string
     */
    private function createCustomFields(array $item): string
    {
        $result = [];

        if (array_key_exists('custom_fields', $item) && $item['custom_fields'] !== '') {
            $result = json_decode($item['custom_fields'], true);
        }

        if (is_null($this->customFieldsSelection)) {
            return json_encode($result);
        }

        foreach ($this->customFieldsSelection as $localKey => $customField) {
            if (array_key_exists($localKey, $item)) {
                $result[$customField] = $item[$localKey];
            }
        }

        return json_encode($result);
    }

    /**
     * @param array $article
     *
     * @return array
     */
    private function removeUnusedFields(array $article)
    {
        foreach ($this->fieldsRemoveFromCSV as $remove) {
            if (!array_key_exists($remove, $article)) {
                continue;
            }

            unset($article[$remove]);
        }

        return $article;
    }

    /**
     * @param string $str
     *
     * @return string
     */
    private function removeSpecialChars(string $str)
    {
        $str = str_replace("\n", " ", $str);
        return str_replace("\r", " ", $str);
    }
}
