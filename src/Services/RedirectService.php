<?php
/**
 * Implemented by Media Motion AG team https://www.mediamotion.ch
 *
 * @copyright Media Motion AG https://www.mediamotion.ch
 * @license LGPL-3.0+
 * @link https://www.mediamotion.ch
 */

namespace Memo\Importer\Services;

use Shopware\Core\Content\Product\ProductEntity;
use Shopware\Core\Content\Seo\SeoUrlPlaceholderHandlerInterface;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepository;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsFilter;
use Shopware\Core\System\SalesChannel\SalesChannelContext;
use Shopware\Core\System\SalesChannel\Context\AbstractSalesChannelContextFactory;
use Shopware\Core\System\SalesChannel\Context\SalesChannelContextService;

/**
 * Class ImporterService
 *
 * @package MemoImporter\Services
 */
class RedirectService
{
    private EntityRepository $productRepository;

    private AbstractSalesChannelContextFactory $salesChannelContextFactory;

    private Context $context;

    private EntityRepository $salesChannelRepository;

    private ?SalesChannelContext $salesChannelContext;

    private EntityRepository $languageRepository;

    private EntityRepository $seoUrlRepository;

    private SeoUrlPlaceholderHandlerInterface $seoUrlPlaceholderHandler;

    /**
     * @param EntityRepository $productRepository
     * @param EntityRepository $salesChannelRepository
     * @param EntityRepository $languageRepository
     * @param AbstractSalesChannelContextFactory $salesChannelContextFactory
     * @param SeoUrlPlaceholderHandlerInterface $seoUrlPlaceholderHandler
     * @param EntityRepository $seoUrlRepository
     */
    public function __construct(
        EntityRepository $productRepository,
        EntityRepository $salesChannelRepository,
        EntityRepository $languageRepository,
        AbstractSalesChannelContextFactory $salesChannelContextFactory,
        SeoUrlPlaceholderHandlerInterface $seoUrlPlaceholderHandler,
        EntityRepository $seoUrlRepository
    ) {
        $this->productRepository = $productRepository;
        $this->salesChannelContextFactory = $salesChannelContextFactory;
        $this->seoUrlPlaceholderHandler = $seoUrlPlaceholderHandler;
        $this->salesChannelRepository = $salesChannelRepository;
        $this->languageRepository = $languageRepository;
        $this->seoUrlRepository = $seoUrlRepository;

        $this->context = Context::createDefaultContext();
    }

    /**
     * Run the conversion of the Import.
     *
     * @param string $inputPath
     * @param string $outputPath
     * @param string $salesChannelName
     * @param string $languageName
     * @param bool $permanentRedirects
     *
     * @return array{'failed' => boolean, 'errors' => []}
     */
    public function execute(string $inputPath, string $outputPath, string $salesChannelName, string $languageName, bool $permanentRedirects)
    {
        $csvResultVariants = [];
        $csvData = $this->getCSVArray($inputPath);

        $this->createSalesChannelContextByName($salesChannelName, $languageName);
        $csvResult = $this->getRedirects($csvData);

        return $this->createRedirectFile($csvResult, $outputPath, $permanentRedirects);
    }

    /**
     * @param array $data
     *
     * @return array
     */
    public function getRedirects(array $data)
    {
        $result = [];

        foreach ($data as $item) {
            $url = parse_url($item['link']);
            $targetUrl = $this->generateSEORoute($item['type'], ['productNumber' => $item['productNumber']]);

            if (!$targetUrl) {
                continue;
            }

            $result[] = [
                'old' => $url['path'] . (array_key_exists('query', $url) ? '?' . $url['query'] : ''),
                'new' => $targetUrl,
            ];
        }

        return $result;
    }

    /**
     * @param array $data
     * @param string $outputPath
     * @param bool $permanentRedirects
     *
     * @return void
     */
    private function createRedirectFile(array $data, string $outputPath, bool $permanentRedirects)
    {
        $fileData = '';
        $result = [
            'failed' => false,
            'errors' => [],
            'file' => false
        ];
        $redirectType = $permanentRedirects ? '301' : '302';

        usort($data, function($a, $b) {
            $urlA = parse_url($a['old']);
            $urlB = parse_url($b['old']);

            if (array_key_exists('query', $urlA) && array_key_exists('query', $urlB)) {
                return 0;
            }

            if (array_key_exists('query', $urlA)) {
                return -1;
            }

            if (array_key_exists('query', $urlB)) {
                return 1;
            }

            return $urlA['path'] <=> $urlB['path'];
        });

        foreach ($data as $item) {
            if ($item['old'] === '') {
                continue;
            }

            if (!$item['new']) {
                $result['failed'] = true;
                $result['errors'][] = 'No Product found for URL: ' . $item['old'];

                continue;
            }

            $oldUrl = parse_url($item['old']);

            if (array_key_exists('query', $oldUrl)) {
                $fileData .= 'RewriteCond %{QUERY_STRING} ^' . $oldUrl['query'] . '$ [NC]' . PHP_EOL;
            }

            $fileData .= 'RewriteRule ^' . ltrim($oldUrl['path'], '/') . '$ ' . $item['new'] . '? [R=' . $redirectType . ',L]' . PHP_EOL;
            $fileData .= PHP_EOL;
        }

        if ($fileData !== '') {
            $writeResult = file_put_contents($outputPath, $fileData);

            if ($writeResult === false) {
                $result['failed'] = true;
                $result['errors'][] = 'Error while writing to file';

                return $result;
            }

            $result['file'] = $outputPath;
        }

        return $result;
    }

    /**
     * Creates a array from a given CSV with title headers set as keys.
     *
     * @param string $csvPath
     *
     * @return array
     */
    private function getCSVArray(string $csvPath)
    {
        $csvFile = fopen($csvPath, 'r');
        $csvData = [];
        $index = 0;
        $indexes = [];

        if (!$csvFile) {
            return [];
        }

        while (false !== ($row = fgetcsv($csvFile, 0, ';', '"', "\n"))) {
            if ($index === 0) {
                $indexes = $row;
            } else {
                $data = [];

                foreach ($indexes as $key => $indexed) {
                    $data[$indexed] = $row[$key];
                }

                if (count($data) !== 0) {
                    $csvData[] = $data;
                }
            }

            $index++;
        }

        return $csvData;
    }

    /**
     * Finds the first value inside an Array.
     *
     * @param array $array
     *
     * @return mixed|null
     */
    private function getFirstValueInArray(array $array)
    {
        $tmp = array_values($array);

        if (!array_key_exists(0, $tmp)) {
            return null;
        }

        return $tmp[0];
    }

    /**
     * ToDo: Implement.
     *
     * @param string $salesChannelName
     * @param string $languageName
     *
     * @return SalesChannelContext
     */
    private function createSalesChannelContextByName(string $salesChannelName, string $languageName)
    {
        $salesChannelId = $this->getSalesChannelByName($salesChannelName)->getId();
        $languageId = $this->getLanguageByName($languageName)->getId();

        return $this->createSalesChannelContext($salesChannelId, $languageId);
    }

    /**
     * @param string $name
     *
     * @return mixed|null
     */
    private function getSalesChannelByName(string $name)
    {
        $criteria = (new Criteria())
            ->addAssociation('translations')
            ->addFilter(new EqualsFilter('translations.name', $name));

        return $this->salesChannelRepository->search($criteria, $this->context)->first();
    }

    /**
     * @param string $name
     *
     * @return mixed|null
     */
    private function getLanguageByName(string $name)
    {
        $criteria = (new Criteria())
            ->addFilter(new EqualsFilter('name', $name));

        return $this->languageRepository->search($criteria, $this->context)->first();
    }

    /**
     * @param string $name
     *
     * @return array
     */
    private function getSeoUrlByName(string $name)
    {
        $result = [];
        $criteria = (new Criteria())
            ->addFilter(new EqualsFilter('name', $name));
        $host = '';

        $products = $this->productRepository->search($criteria, $this->context)->getEntities();

        foreach ($products as $product) {
            $parameter = ['productId' => $product->getId()];

            $raw = $this->seoUrlPlaceholderHandler->generate('frontend.detail.page', $parameter);
            $result[] = $this->seoUrlPlaceholderHandler->replace($raw, $host, $this->salesChannelContext);
        }

        return $result;
    }

    private function generateSEORoute($name, array $parameters = [])
    {
        switch($name) {
            case 'product':
                $name = 'frontend.detail.page';
                $criteria = (new Criteria())
                    ->addFilter(new EqualsFilter('productNumber', $parameters['productNumber']));
                $result = $this->productRepository->search($criteria, $this->salesChannelContext->getContext());

                if (!$result->count()) {
                    return null;
                }

                /** @var ProductEntity $product */
                $product = $result->first();

                if (!$product->getActive()) {
                    return null;
                }

                $parameters['productId'] = $product->getId();

                unset($parameters['productNumber']);
                break;
            case 'category':
                $name = 'frontend.navigation.page';
                break;
        }

        $raw = $this->seoUrlPlaceholderHandler->generate($name, $parameters);

        return $this->seoUrlPlaceholderHandler->replace($raw, '', $this->salesChannelContext);
    }

    /**
     * @param string $salesChannelId
     * @param string $languageId
     *
     * @return SalesChannelContext
     */
    private function createSalesChannelContext(string $salesChannelId, string $languageId)
    {
        if (!isset($this->salesChannelContext)) {
            $this->salesChannelContext = $this->salesChannelContextFactory->create('', $salesChannelId, [SalesChannelContextService::LANGUAGE_ID => $languageId]);
        }

        return $this->salesChannelContext;
    }
}
